let num1, num2, mathOperator;

do {
    num1 = prompt('Enter first number', validateNumber(num1) ? '' : num1);
    num2 = prompt('Enter second number', validateNumber(num2) ? '' : num2);
} while (validateNumber(num1) || validateNumber(num2));

do {
    mathOperator = prompt('Enter one of the proposed operators (+, -, /, *)');
} while (!mathOperator || !mathOperator.match(/[\-, +, *, /]/g));

handleOperation(num1, num2, mathOperator);

function handleOperation(num1, num2, operator) {
    num1 = Number(num1);
    num2 = Number(num2);

    switch (operator) {
        case '+':
            console.log(num1 + num2);
            break;
        case '-':
            console.log(num1 - num2);
            break;
        case '*':
            console.log(num1 * num2);
            break;
        case '/':
            !num2 ? console.log('You cannot divide by zero. (Infinity)') : console.log(num1 / num2);
            break;
    }
}

function validateNumber(num) {
    return isNaN(Number(num)) || !num;
}